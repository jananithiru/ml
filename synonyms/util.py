import numpy as np
import io
import csv

def read_file(file_path):
    dataset = np.genfromtxt(open(file_path,'r'), delimiter=',', dtype='f8')[0:]
    return dataset