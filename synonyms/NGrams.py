__author__ = 'janani'
import TestWordNet
from TestWordNet.collocations import *

def method2():

    words = TestWordNet.word_tokenize("History of United States History of American")

    my_bigrams = TestWordNet.bigrams(words)
    my_trigrams = TestWordNet.trigrams(words)

    pairs = [ " ".join(pair) for pair in TestWordNet.bigrams(words)]
    pairs2 = [ " ".join(pair) for pair in TestWordNet.trigrams(words)]

    print(words)
    print(pairs)
    print(pairs2)

    #Substitue synonyms in the these words

    fdist = TestWordNet.FreqDist(my_bigrams)
    for k,v in fdist.items():
        print(k,v)

def method1():
    line = ""
    open_file = open('a_text_file','r')
    for val in open_file:
        line += val
    tokens = line.split()
    bigram_measures = TestWordNet.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokens)
    finder.apply_freq_filter(3)
    finder.ngram_fd.viewitems()
    #finder.nbest(bigram_measures.pmi, 100)
