__author__ = 'janani'

'''## First download the dataset you want using their identifiers e.g., brown
> Open Python3
>>> import nltk
>>> nltk.downloader
>>> brown
>>> reuters
>>>
'''

## Identify categories
from nltk.corpus import brown
print("BROWN")
print(brown.categories())


from nltk.corpus import reuters
print("REUTERS")
print(reuters.categories())

''' # http://compling.hss.ntu.edu.sg/omw/
# How to get this for different languages
# Ensure every language you want to check in is downloaded
'''

from nltk.corpus import indian
print(indian.words('hindi.pos'))
