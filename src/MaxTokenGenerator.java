import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import sun.invoke.empty.Empty;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by janani on 11/19/15.
 */
public class MaxTokenGenerator {

    public static void main(String[] args) {

        String filename = "/home/janani/Downloads/labeled_data.xls - Sheet2.csv";

        List<String> array1 = null;
        List<String> array2 = null;
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.useDelimiter(",");

            array1 = new ArrayList<String>();
            array2 = new ArrayList<String>();

            while (scanner.hasNextLine()) {
                String[] a = scanner.nextLine().split(",");
                array1.add(a[0]);
                array2.add(a[1]);

            }
            scanner.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        if(array1.size() != array2.size()) {
            System.out.println("Something is amiss, size of the two arrays has to be same");
        }
        else {
            List<Integer> maxTokenScores = runMaxTokenOverlap(array1, array2);

            for (Integer score: maxTokenScores){
                System.out.println(score);
                // Write scores to file
            }
        }
    }

    private static List<Integer> runMaxTokenOverlap(List<String> array1, List<String> array2) {
        List<Integer> maxTokenScores = new ArrayList(array1.size()) ;
        for (int i=0; i < array1.size(); i++) {
            int v = getMaxOverlappingTokens(array1.get(i), array2.get(i));
            maxTokenScores.add(i, new Integer(v));
        }
        return maxTokenScores;
    }


    public static int getMaxOverlappingTokens(String title1, String title2) {

        if (title1 == null || title2 == null)
                return 0;

        String regex = "\\s*[^a-zA-Z]+\\s*";

        String[] tokens1 = title1.split(regex);
        String[] tokens2 = title1.split(regex); 

        int score = 0;
        for (int j=0; j < tokens1.length; j++) {
            for(int i=0; i < tokens2.length; i++){
                if (tokens1[j].equals(tokens2[i]))
                    score++;
            }
        }
        return score;
    }
}
