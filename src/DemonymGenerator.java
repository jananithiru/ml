import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * Created by janani on 11/19/15.
 */
public class DemonymGenerator {

    String demonymFilename;
    String trainingDataFileName;
    HashMap<String, List<String>> demonymDictionary;


    DemonymGenerator(String trainingDataFileName, String demonymFilename) {
        this.demonymFilename = demonymFilename;
        this.trainingDataFileName = trainingDataFileName;
        this.demonymDictionary = new HashMap<String, List<String>>();
        loadDemonymDict(this.demonymDictionary, this.demonymFilename);
    }

    public static void main(String[] args) {

        DemonymGenerator dm = new DemonymGenerator("/home/janani/ML/ml/data/pairs.csv","/home/janani/ML/ml/data/All_Demonyms.csv");
        List<String> array1 = null;
        List<String> array2 = null;
        try {
            Scanner scanner = new Scanner(new File(dm.trainingDataFileName));
            scanner.useDelimiter(",");

            array1 = new ArrayList<String>();
            array2 = new ArrayList<String>();

            while (scanner.hasNextLine()) {
                String[] a = scanner.nextLine().split(",");
                array1.add(a[0]);
                array2.add(a[1]);

            }
            scanner.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        if (array1.size() != array2.size()) {
            System.out.println("Something is amiss, size of the two arrays has to be same");
            return;
        }


        /** MAX TOKEN OVERLAP **/
        List<Float> maxTokenScores = runMaxTokenOverlap(array1, array2);
        System.out.println("MAXTOKEN SCORE");
        for (Float score : maxTokenScores) {
            if(score>0)
            System.out.print(score + " ,");
        }

        /** DEMONYM MAX TOKEN OVERLAP **/
        List<Float> demonymScores = getDemonymScores(dm, array1, array2);

        System.out.println("DEMONYM SCORE");
        for (Float score : demonymScores) {
            if(score>0)
                System.out.println(score+" ,");
        }

        // TODO: Write scores to file
    }

    private static List<Float> getDemonymScores(DemonymGenerator dm, List<String> array1, List<String> array2) {
        List<Float> maxTokenDemonymScores = new ArrayList(array1.size());
        for (int i = 0; i < array1.size(); i++) {
            float v = getMaxOverlappingTokensForDemonyms(dm, array1.get(i), array2.get(i));
            maxTokenDemonymScores.add(i, new Float(v));
        }
        return maxTokenDemonymScores;
    }


    public static float getMaxOverlappingTokensForDemonyms(DemonymGenerator dm, String title1, String title2) {

        if (title1 == null || title2 == null)
            return 0;

        String regex = "\\s*[^a-zA-Z]+\\s*";

        String[] tokens1 = title1.split(regex);
        String[] subTitleTokens = title2.split(regex);

        // check if title contains a word which could potentially have demonyms like India, China

        float count = 0;

        // TODO: 4 test cases
        // The key might be in the title
        for (String t : tokens1) {
            if (t.length() > 3) {
                // Look for the word in the hashmap
                List<String> demos = dm.demonymDictionary.containsKey(t) ?
                        dm.demonymDictionary.get(t) : null;
                if (demos != null) {
                    for (String st : subTitleTokens) {
                        // check if subtitle then contains a demonym Indian, Chinese etc
                        if (demos.contains(st))
                            // if it does then add to the count of max overlapping tokens
                            count++;
                    }
                }
            }
        }
        // The key might be in the subtitle
        for (String st : subTitleTokens) {
            if (st.length() > 3) {
                // Look for the word in the hashmap
                List<String> demos = dm.demonymDictionary.containsKey(st) ?
                        dm.demonymDictionary.get(st) : null;
                if (demos != null) {
                    for (String t : tokens1) {
                        if (demos.contains(t))
                            count++;
                    }
                }
            }
        }

        return count;
    }


   // private HashMap<String, List<String>> loadDemonymDict(HashMap<String, List<String>> dict, String fileName) {
   private void loadDemonymDict(HashMap<String, List<String>> dict, String fileName) {
        try {
            Scanner scanner = new Scanner(new File(fileName));
            scanner.useDelimiter(",");

            while (scanner.hasNextLine()) {
                String[] a = scanner.nextLine().split(",");
                String key = a[0].trim();
                List<String> values = new ArrayList<String>();
                for (int i = 1; i < a.length; i++){
                    values.add(a[i].trim());
                }
                dict.put(key, values);
            }
            scanner.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    private static List<Float> runMaxTokenOverlap(List<String> array1, List<String> array2) {
        List<Float> maxTokenScores = new ArrayList(array1.size());
        for (int i = 0; i < array1.size(); i++) {
            float v = getMaxOverlappingTokens(array1.get(i), array2.get(i));
            maxTokenScores.add(i, new Float(v));
        }
        return maxTokenScores;
    }


    public static float getMaxOverlappingTokens(String title1, String title2) {

        if (title1 == null || title2 == null)
            return 0;

        String regex = "\\s*[^a-zA-Z]+\\s*";

        String[] tokens1 = title1.split(regex);
        String[] subTitletokens = title2.split(regex);

        float score = 0;
        for (int j = 0; j < tokens1.length; j++) {
            for (int i = 0; i < subTitletokens.length; i++) {
                if (tokens1[j].equals(subTitletokens[i]))
                    score++;
            }
        }
        return score;
    }

}


