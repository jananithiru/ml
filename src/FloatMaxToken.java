import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by janani on 11/19/15.
 */
public class FloatMaxToken {

    public static void main(String[] args) {

        String filename = "/data/All_Demonyms";

        List<String> array1 = null;
        List<String> array2 = null;
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.useDelimiter(",");

            array1 = new ArrayList<String>();
            array2 = new ArrayList<String>();

            while (scanner.hasNextLine()) {
                String[] a = scanner.nextLine().split(",");
                array1.add(a[0]);
                array2.add(a[1]);

            }
            scanner.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        if (array1.size() != array2.size()) {
            System.out.println("Something is amiss, size of the two arrays has to be same");
            return;
        }

        List<Float> maxTokenScores = runMaxTokenOverlap(array1, array2);
        //List<

        for (Float score : maxTokenScores) {
            System.out.println(score);
            // TODO: Write scores to file
        }
    }

    private static List<Float> runMaxTokenOverlap(List<String> array1, List<String> array2) {
        List<Float> maxTokenScores = new ArrayList(array1.size());
        for (int i = 0; i < array1.size(); i++) {
            int v = getMaxOverlappingTokens(array1.get(i), array2.get(i));
            maxTokenScores.add(i, new Float(v));
        }
        return maxTokenScores;
    }


    public static int getMaxOverlappingTokens(String title1, String title2) {

        if (title1 == null || title2 == null)
            return 0;

        String regex = "\\s*[^a-zA-Z]+\\s*";
        //Pattern commaPattern = Pattern.compile("[^A-Za-z0-9]") ;
        String[] tokens1 = title1.split(regex);
        String[] tokens2 = title1.split(regex);

        int score = 0;
        for (int j = 0; j < tokens1.length; j++) {
            for (int i = 0; i < tokens2.length; i++) {
                if (tokens1[j].equals(tokens2[i]))
                    score++;
            }
            //scores.set(j,score);
        }
        return score;
    }
}
